﻿$(document).ready(function () {
    // starting main Carousel
    $('#mainCarousel').carousel({
        pause: true,
        interval: false
    });


     
    // starting News Carousel
    var owl = $('.owl-carousel');

    if ($('body').attr('dir') == "rtl") {
        owl.owlCarousel({
            nav: false,
            rtl: true,
            loop: true,
            margin: 10,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                960: {
                    items: 2
                },
                1315: {
                    items: 3
                }
            }
        });
    } else {
        owl.owlCarousel({
            nav: false,
           
            loop: true,
            margin: 10,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                960: {
                    items: 2
                },
                1315: {
                    items: 3
                }
            }
        });
    }

    

    // copy fist owl element to the big first-cards-img and then hide it if screen size > 991
    if ($(window).width() > 991) {
        var owlActiveEls = $('.owl-item.active').eq(0).find('.cards-img-container').html();
        $('.first-cards-img-container').html(owlActiveEls);
        $('.first-cards-img-container').find('.first-cards-paragraph').removeClass("d-none");
        $('.owl-item.active').eq(0).addClass("d-none");
    }

    // carousel on change if screen size > 991 copy fist owl element to the big first-cards-img and then hide it
    owl.on('changed.owl.carousel', function (event) {
        if ($(window).width() >= 991) {
            setTimeout(function () {
                $(".owl-item").removeClass("d-none");
                var owlActiveEls = $('.owl-item.active').eq(0).find('.cards-img-container').html();
                $('.first-cards-img-container').html(owlActiveEls);
                $('.first-cards-img-container').find('.first-cards-paragraph').removeClass("d-none");
                $('.owl-item.active').eq(0).addClass("d-none");
            }, 0.5);
        }
    });

    // on window resize if screen size > 991 copy fist owl element to the big first-cards-img and then hide it
    $(window).resize(function () {
        if ($(window).width() > 991) {
            var owlActiveEls = $('.owl-item.active').eq(0).find('.cards-img-container').html();
            $('.first-cards-img-container').html(owlActiveEls);
            $('.owl-item.active').eq(0).addClass("d-none");
        } else {
            $(".owl-item").removeClass("d-none");
        }
    });

    // custom nav for owl scrolling 
    $(".js-customNextBtn").on("click", function () {
        owl.trigger('next.owl.carousel');
    });
    $(".js-customPrevBtn").on("click", function () {
        owl.trigger('prev.owl.carousel');
    });

    // custom play icon for video
    $('.video-js').parent().click(function () {
        if ($(this).children(".video-js").get(0).paused) {
            $(this).children(".video-js").get(0).play();
            $(this).children(".play-btn").fadeOut();
        } else {
            $(this).children(".video-js").get(0).pause();
            $(this).children(".play-btn").fadeIn();
        }
    });

    // get total of slides number
    $('#js-total-slides').html($('.carousel-item').length);

    // get next active slide number + pausing all videos
    $("#mainCarousel").on('slide.bs.carousel', function (evt) {
        $('#js-current-slid').html($(evt.relatedTarget).index() + 1);

        //pausing all videos
        $('.video-js').each(function () {
            $(this).get(0).pause();
            $(this).siblings(".play-btn").fadeIn();
        });
    });

    // init Parallexing : visit paroller.js
    $('.paroller').paroller();

    var currentIndex;

    $(".available-img-in-gal").click(function () {

        if ($(window).width() >= 768) {

            var modalImg = document.getElementById("overlay_Image");
            modalImg.src = this.src;
            $('body').css('overflow', 'hidden');
            $("#prevGalBtn").addClass("btn-disabled");
            $("#nextGalBtn").removeClass("btn-disabled");
            on();
            var listItem = $(this);
            currentIndex = $(".available-img-in-gal").index(listItem);

        }
    });


    var imagesSrc = [];
    $(".available-img-in-gal").each(function () {
        imagesSrc.push($(this).attr('src'));
    });
    //$(imagesSrc).each(function (i, v) {
    //    console.log(imagesSrc[i]);

    //});

    $(".close_overlay").click(function () {
        $('body').css('overflow', 'auto');
        off();
    });

    $("#prevGalBtn").click(function () {

        if (currentIndex === 11) {
            $("#nextGalBtn").removeClass("btn-disabled");
        }
        //alert(currentIndex);
        if (currentIndex === 1) {
            //alert("you are in the first");
            $("#prevGalBtn").addClass("btn-disabled");
        }
        if (currentIndex === 0) {
            //alert("you are in the first");
            
        } else {
            var modalImg = document.getElementById("overlay_Image");
            currentIndex = currentIndex - 1;
            modalImg.src = imagesSrc[currentIndex];
        }
    });
    $("#nextGalBtn").click(function () {
        $("#prevGalBtn").removeClass("btn-disabled");
        // alert(currentIndex);
        if (currentIndex === 10) {
            $("#nextGalBtn").addClass("btn-disabled");
        }

        if (currentIndex === 11) {
            //alert("you are in the last , call Ajax");

        } else {
            var modalImg = document.getElementById("overlay_Image");
            currentIndex = currentIndex + 1;
            modalImg.src = imagesSrc[currentIndex];
        }

    });




});




// show scroll up btn when reach end of page 
var topOfOthDiv = $(".bg-green").offset().top;
$(window).scroll(function () {
    if ($(window).scrollTop() > topOfOthDiv) {
        $('.scrollup-btn').fadeIn();
    } else {
        $('.scrollup-btn').fadeOut();
    }
});

//$(window).scroll(function () {
//    if ($(this).scrollTop()) {
//        $('#toTop').fadeIn();
//    } else {
//        $('#toTop').fadeOut();
//    }
//});

// scroll to Top
$('.scrollup-btn').click(function () {
    $("html, body").animate({ scrollTop: 0 }, 100);
    return false;
});



function on() {
    $("#overlay").fadeIn();
    $("html").css("overflow", "hidden");
    //$(this).find(".img-description-index ").css("height","50px");
}

function off() {
    $("#overlay").fadeOut();
    $("html").css("overflow", "auto");
    //$(this).find(".img-description-index ").css("height", "0px");

}



// on window resize if screen size > 991 copy fist owl element to the big first-cards-img and then hide it
$(".gal-img-big-container").on("click",
    function () {
        if ($(window).width() < 767) {
            $(this).find(".img-description-index").addClass("img-description-index-m-show");

            var $this = $(this);
            setTimeout(function () {
                $this.find(".img-description-index").removeClass("img-description-index-m-show");

            }, 4000);

        }
    });


$(".gal-img-small-container").on("click",
    function () {
        if ($(window).width() < 767) {
            $(this).find(".img-description-index").addClass("img-description-index-m-show");

            var $this = $(this);
            setTimeout(function () {
                $this.find(".img-description-index").removeClass("img-description-index-m-show");

            }, 4000);
        }
    });

